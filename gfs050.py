from gfs import *

class Gfs050(Gfs):
    """
    Define a 0.50 GFS class
    """

    def __init__(self):
        super(Gfs050, self).__init__()

        import gfs050Data

        self._resolution = 0.5
        self.level = gfs050Data.level
        self.variable = gfs050Data.variable
        self.region = gfs050Data.region

    def url (self, experiment_wrf, actual_date, actual_forecast):
        url = super(Gfs050, self).url()

        file_ = 'filter_gfs_0p50.pl?file=gfs.t%sz.pgrb2full.0p50.f%s&'%\
        (experiment_wrf.start_date.strftime("%H"), str(actual_forecast).zfill(3))

        grib_url = reduce( lambda x, y: x+'&'+y , ['lev_'+x+'=on'  for x in self.level.keys() if self.level[x] ])
        grib_url += '&'+reduce( lambda x, y: x+'&'+y , ['var_'+x+'=on'  for x in self.variable.keys() if self.variable[x] ])

        grib_url += "&subregion=&"+ reduce( lambda x, y: x+'&'+y , ['%s=%s'%(x,self.region[x]) for x in self.region.keys() if self.region[x] ])

        grib_url += '&dir=%2Fgfs.'+experiment_wrf.start_date.strftime("%Y%m%d%H")

        return url, file_, grib_url

    def resolution(self):
        return '0.50'

    def name(self):
        return 'gfs_operational'


    def forcing_patern(self):
        return self.name()+"_0.50_????????_??_???.grib2"
