#!/usr/bin/env python

import config
import argparse
import ExperimentWRF
from datetime import datetime, timedelta
import wrfLogger
import logging
import utils

class Gfs(object):

    def __init__(self):
        self.experiment_wrf = None

    def check(self, out_file):

        import os.path
        import subprocess

        if not os.path.exists(out_file):
            config.LOGGER.error('GFS failed: file not exits')
            return False

        #empty
        command = ['wgrib2 '+out_file]
        file_uncompleted = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()
        # print command
        if file_uncompleted == '':
            config.LOGGER.error('GFS failed: empty')
            return False



        #file is complete
        command = ['wgrib2 '+out_file+' 2>&1 | grep "FATAL ERROR" | wc -l']
        file_uncompleted = int(subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read())
        # print command
        if file_uncompleted == 1:
            config.LOGGER.error('GFS failed: uncompleted')
            return False


        ####################################################3


        # date in file is the analisys time
        start_date = self.experiment_wrf.start_date.strftime('%Y%m%d%H')
        command = ['wgrib2 '+out_file+' 2> /dev/null | head -n 1 | awk -F: \'{ print $3}\' | awk -F"=" \'{ print $2}\'']
        file_date = int(subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read())

        # print command
        if not int(start_date) == file_date:
            config.LOGGER.error('GFS failed: anl time wrong')
            return False

        ####################################################3
        # pdb.set_trace()
        # the hour is anl or be in the forecast hours
        command = ['wgrib2 '+out_file+' | head -n 1 | awk -F: \'{ print $6}\' | awk -F" " \'{ print $1}\'']
        tmp = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()

        if tmp == 'anl\n':
            forecast_time = 0
        else:
            forecast_time = int(tmp)

        if not (forecast_time in range(0, self.experiment_wrf.forecast_hours()+1, 3)):
            config.LOGGER.error('GFS failed: hours wrong')
            return False
        ####################################################3

        return True

    def host(self):
        return "nomads.ncep.noaa.gov"

    def url(self):

        return "http://"+self.host()+"/cgi-bin/"


    def download(self):
        """
        get gfs forcing from ncep server ussing curl using experiment_wrf parameters
        """

        import os
        import time
        experiment_wrf = self.experiment_wrf

        actual_date = experiment_wrf.start_date

        forecast_hours = experiment_wrf.forecast_hours()
        actual_forecast = 0

        config.LOGGER.info('downloading GFS from nomads.ncep.noaa.gov ')
        try_to_download = 0

        while  actual_forecast <= forecast_hours:

            out_file = config.PATH['tmp']+'/'+self.name_out(actual_forecast)

            # import pdb; pdb.set_trace()
            url, file_, griburl = \
            experiment_wrf.gfs.url(experiment_wrf, actual_date, actual_forecast)

            if experiment_wrf.debug:

                actual_date += \
                timedelta(hours=experiment_wrf.time_delta_input/3600)
                actual_forecast += experiment_wrf.time_delta_input/3600

                print config.PROGRAMS['curl']+' "'+url + file_ + griburl +'" -o '+ out_file

            else:
                if experiment_wrf.gfs.check(out_file):
                    config.LOGGER.info('allready donwload '+out_file)

                    actual_date += \
                    timedelta(hours=experiment_wrf.time_delta_input/3600)
                    actual_forecast += experiment_wrf.time_delta_input/3600

                else:
                    config.LOGGER.info('Front end: downloading '+out_file)
                    utils.safe_run(os.system(config.PROGRAMS['curl']+\
                    ' "'+url + file_ + griburl +'" -o '+ out_file), experiment_wrf)

                    if experiment_wrf.gfs.check(out_file):
                        config.LOGGER.info(out_file+' Done!')
                        actual_date += \
                        timedelta(hours=experiment_wrf.time_delta_input/3600)
                        actual_forecast += experiment_wrf.time_delta_input/3600
                    else:
                        if try_to_download < 5:
                            time.sleep(5)
                            print 'try_to_download '+str(try_to_download)

                            config.LOGGER.info('download failed try '\
                            +str(try_to_download)+' '+out_file)

                            try_to_download += 1

                        else:

                            config.LOGGER.info('after 5 attemps to download '\
                            +str(try_to_download)+' '+out_file+' will continue')

                            actual_date += \
                            timedelta(hours=experiment_wrf.time_delta_input/3600)
                            actual_forecast += experiment_wrf.time_delta_input/3600


    def name_out(self, forecast_hour):

        return (self.experiment_wrf.gfs.name()+"_%s_%s_%s_%s.grib2")%(self.experiment_wrf.gfs.resolution(),\
        self.experiment_wrf.start_date.strftime("%Y%m%d"),\
        self.experiment_wrf.start_date.strftime("%H").zfill(2), str(forecast_hour).zfill(3) )




def forecast_hours(value):
    ivalue = int(value)
    assert ivalue % 3 == 0, "%s is an invalid forecast hour"%value
    return ivalue


def mkdate(datestr):
    return datetime.strptime(datestr, '%Y%m%d-%H')
def main():
    import gfs025
    import gfs050
    import gfsDPA

    parser = argparse.ArgumentParser()

    parser.add_argument('anl_time', type = forecast_hours,
    help = 'must be 0, 6, 12 or 18', choices = [0, 6, 12, 18])

    parser.add_argument('--start_date', type = mkdate, \
    help = 'format YYYYMMDD')

    parser.add_argument('forecast_hours', type = forecast_hours,
    help = 'hour must be 3, 6, 9, 12, etc')

    parser.add_argument('resolution', type=float,  choices=[0.5, 0.25], \
    help="0.5 or 0.25")
    parser.add_argument('profile', choices=['operational', 'dpa'], \
    help="data for operational or for dpa")

    parser.add_argument('--log_level', help="log_level could be: debug info \
    warning error or critical")
    parser.add_argument('--debug', action='store_true', default=False)

    args = parser.parse_args()

    if not args.start_date:

        start_date = datetime(\
            datetime.today().year, \
            datetime.today().month, \
            datetime.today().day,
            args.anl_time)
    else:
        start_date = args.start_date


    if args.profile == 'operational':
        if args.resolution == 0.25:
            my_gfs = gfs025.Gfs025()
        if args.resolution == 0.5:
            my_gfs = gfs050.Gfs050()

    if args.profile == 'dpa':
        if args.resolution == 0.25:
            my_gfs = gfsDPA.GfsDPA()
        if args.resolution == 0.5:
            print 'not implemented yeat'
            return


    experiment_wrf = ExperimentWRF.ExperimentWRF(start_date,\
    start_date + timedelta(hours=args.forecast_hours), config.LOGGER, my_gfs)

    experiment_wrf.debug = args.debug

    my_gfs.experiment_wrf = experiment_wrf

    config.LOGGER = wrfLogger.setup(logging.getLogger(),experiment_wrf)

    my_gfs.download()

if __name__ == "__main__":
    main()
