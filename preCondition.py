#!/usr/bin/env python

import config
import logging
import wrfLogger


class PreCondition(object):


    def __init__(self, experiment_wrf):
        # self.status_msg = None
        self.experiment_wrf = experiment_wrf
        self.status = True

    def status_msg(self):
        return "Status msg moke: TO BE implemented"

    def check(self):
        pass




class PreConditionUngrib(PreCondition):
    pass

class PreConditionMetgrid(PreCondition):
    pass


class PreConditionGeogrid(PreCondition):
    pass

class PreConditionReal(PreCondition):
    pass

class PreConditionWrf(PreCondition):
    pass
