import unittest

import config
import postCondition
import ExperimentWRF
from datetime import datetime, timedelta
import gfs
import shutil
import wrfLogger
import logging

class postConditionTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(postConditionTest, self).__init__(*args, **kwargs)
        start_date = datetime(2016, 4, 29, 12)
        end_date = datetime(2016, 4, 29, 12)

        my_gfs = gfs.Gfs050()

        self.experiment_wrf = ExperimentWRF.ExperimentWRF(\
            start_date, \
            end_date,\
            config.LOGGER,\
            my_gfs)

        my_gfs.experiment_wrf = self.experiment_wrf

        config.LOGGER = wrfLogger.setup(logging.getLogger(), \
        self.experiment_wrf, wrfLogger.LEVELS['debug'])

        self.post_condition_real = postCondition.PostConditionReal(self.experiment_wrf)


    def test_real_true(self):

        shutil.copy(\
        './testing_data/pre_post_conditions/rsl.out.0000.true',\
        config.PATH['wrf']+'/rsl.out.0000')
        self.assertTrue(self.post_condition_real.check(self.experiment_wrf))


    def test_real_false(self):

        shutil.copy(\
        './testing_data/pre_post_conditions/rsl.out.0000.false',\
        config.PATH['wrf']+'/rsl.out.0000')
        self.assertFalse(self.post_condition_real.check(self.experiment_wrf))




if __name__ == '__main__':
    unittest.main()
