import os
import wrfLogger

DEBUG = False
LOGGER = None

CORES = 24
HOSTS = 1

TEMPLATE_WPS = 'namelist.wps.operativo.template'
TEMPLATE_INPUT = 'namelist.input.operativo.template'

SUBDOMAINS = 1

VTABLE = {}
VTABLE['gfs_2015'] = 'ungrib/Variable_Tables/Vtable.GFS_new_20150114'
VTABLE['gfs'] = 'ungrib/Variable_Tables/Vtable.GFS'

MPIEXEC = 'mpirun'




PATH = {}
PATH['work'] = "/home/pablohe/Build_WRF_Intel"
PATH['wps'] = PATH['work']+"/WPS"
PATH['wrf'] = PATH['work']+"/WRFV3/test/em_real"
PATH['experimets_results'] = PATH['work']+"/results"
PATH['scripts'] = os.getcwd()
PATH['tmp'] = '/tmp'
PATH['forcing'] = PATH['tmp']
PATH['geog_data_path'] = PATH['work']+"/geog/"

PROGRAMS = dict(
  link_grib = PATH['wps']+"/link_grib.csh",
  geogrid = PATH['wps']+"/geogrid.exe",
  metgrid = PATH['wps']+"/metgrid.exe",
  ungrib = PATH['wps']+"/ungrib.exe",
  real = PATH['wrf']+"/wps.exe",
  wrf = PATH['wrf']+"/wrf.exe",
)

PROGRAMS['curl'] = 'curl'

log_level = wrfLogger.LEVELS['debug']
