#!/usr/bin/env python

import threading
import time
import os


def main(file_out_wrf):
    """
    main of monitor to run the post processing
    """

    def worker():

        while not os.path.isfile(file_out_wrf):
            time.sleep(3)
            print 'no no esta...'

        print 'esta!'

        return
    threads = list()
    t = threading.Thread(target=worker)
    threads.append(t)
    t.start()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('file_out_wrf')
    args = parser.parse_args()

    main(args.file_out_wrf)
