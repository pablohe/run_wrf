import sys
import subprocess
import config

def safe_run_subprocess(command, experiment_wrf):

    if experiment_wrf.debug:
        print str(command)
    else:
        try:
            config.LOGGER.info(command)
            retcode = subprocess.call(command, shell=True)
        except OSError as e:
          config.LOGGER.error('Jim we have a problem: '+command+" "+e)



def safe_run(command, experiment_wrf):


    config.LOGGER.info(str(command))
    if experiment_wrf.debug:
        print str(command)
    else:
        command

#    try:
#      command
#    except OSError:
#      logger.error('Jim we have a problem: '+command)
