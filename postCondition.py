#!/usr/bin/env python

import config
import os
import logging
import wrfLogger

class PostCondition(object):

    def __init__(self, experiment_wrf):
        self.error_msg = None
        self.experiment_wrf = experiment_wrf
        self.status = True


    def check(self):
        pass

    def status_msg(self):
        return "Status msg moke: TO BE implemented"


class PostConditionUngrib(PostCondition):
    pass

class PostConditionGeogrid(PostCondition):
    pass

class PostConditionMetgrid(PostCondition):
    pass



class PostConditionReal(PostCondition):
    # crep en rsl.out.0000 == "SUCCESS COMPLETE REAL_EM INIT"

    def check(self):

        import subprocess
        command = ['grep "SUCCESS COMPLETE REAL_EM INIT" '\
        +config.PATH['wrf']+'/rsl.out.0000 2>&1 | wc -l']

        self.status = bool(int(subprocess.Popen(command, shell=True, \
        stdout=subprocess.PIPE).stdout.read().rstrip()))


    def status_msg(self):
        ret = "Real OK" if self.status else "Real FAILED"
        return ret


class PostConditionWrf(PostCondition):

    def check(self):
        import subprocess

        command = ['grep "SUCCESS COMPLETE REAL_EM INIT" '\
        +config.PATH['wrf']+'/rsl.out.0000 2>&1 | wc -l']

        self.status = bool(int(subprocess.Popen(command, shell=True, \
        stdout=subprocess.PIPE).stdout.read().rstrip()))

    def status_msg(self):
        ret = "WRF OK" if self.status else "WRF FAILED"
        return ret
