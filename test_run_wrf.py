#!/usr/bin/python

import unittest
import ExperimentWRF

import config
import datetime
import run_wrf
import wrfLogger
import logging

import os
import glob

# TODO hace mas expetimentos

start_date = datetime.datetime(2016,01,25,00)
end_date = datetime.datetime(2016,01,25,18)

my_experiment = ExperimentWRF.ExperimentWRF(start_date, end_date, \
config.LOGGER, config.FORCING_PREFIX)

config.LOGGER = wrfLogger.setup(logging.getLogger(), \
my_experiment, wrfLogger.LEVELS['debug'] )


class TestRunWRF(unittest.TestCase):

    def test_clean_environment_WPS(self):
        #  met_em* GRIBFILE.??? FILE:* PFILE* *.log"

        # configure for safe enviroment and copy fake testing data
        config.PATH['wps'] = '/tmp'
        os.chdir(config.PATH['scripts']+"/testing_data/clean_environment_WPS")
        os.system( ('cp %s/* %s/')%\
        (config.PATH['scripts']+"/testing_data/clean_environment_WPS",config.PATH['wps']))

        run_wrf.clean_environment_WPS(my_experiment)

        # should not find files
        ret = []
        for elem in ['met_em*', 'GRIBFILE.???', 'FILE:*', 'PFILE*', '*.log']:
            ret += glob.glob('./'+elem)

        assert len( ret ) == 0


    def test_clean_environment_WRF(self):

        # configure for safe enviroment and copy fake testing data
        config.PATH['wps'] = '/tmp'

        os.chdir( config.PATH['scripts']+"/testing_data/clean_environment_WRF")
        os.system('cp * '+ config.PATH['wps']+'/')


        run_wrf.clean_environment_WRF(my_experiment)

        # should not find files
        ret = []
        for elem in ['rsl*', 'nohup.out', 'wrfbdy_d01', 'wrfinput_d0*', 'met_em*']:
            ret += glob.glob('./'+elem)

        assert len( ret ) == 0



    def test_prepare_forcing_for_preprocessing(self):

        config.PATH['wps'] = '/tmp'
        config.PATH['forcing'] = config.PATH['scripts']+\
        "/testing_data/prepare_forcing_for_preprocessing/"


        run_wrf.prepare_forcing_for_preprocessing(my_experiment)

        how_many_files = (my_experiment.end_date-my_experiment.start_date).\
        total_seconds()/3600

        how_many_files = 1+how_many_files/(my_experiment.time_delta_input/3600)

        num_files = len(glob.glob(config.PATH['wps']+'/'+my_experiment.forcing_patern))

        # print 'num '+str(num_files)
        # print 'how_many_files '+str(how_many_files)

        assert num_files == int(how_many_files)

    def test_get_met(self):

        # prepare Environment
        config.PATH['wrf'] = '/tmp/em_real'
        config.PATH['wps'] = '/tmp/WPS'

        os.system('mkdir '+config.PATH['wrf']+' '+config.PATH['wps']+' 2>/dev/null')
        os.system(('cp %s/testing_data/get_met/met* %s')\
        %(config.PATH['scripts'],config.PATH['wps']) )

        run_wrf.get_met(my_experiment)

        os.chdir(config.PATH['wrf'])
        assert len( glob.glob('met_em*') ) == 7

        # deberia testear que son los met_em correctos de este experimento?


    def test_run_preprocessing(self):
        pass

    def test_make_namelistINPUT(self):
        pass

    def test_make_namelistWPS(self):
        pass

    def test_make_namelist(self):
        pass

    def test_run_processing(self):
        pass

    def test_make_graphics(self):
        pass

    def test_move_graphics(self):
        pass

    def test_move_output(self):
        pass


if __name__ == '__main__':
    unittest.main()
