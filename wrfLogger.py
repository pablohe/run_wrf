
import logging

LEVELS = { 'debug':logging.DEBUG,
           'info':logging.INFO,
           'warning':logging.WARNING,
           'error':logging.ERROR,
           'critical':logging.CRITICAL,
          }
import config
import os

def setup(logger, my_experiment, log_level=logging.DEBUG):

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # level = wrfLogger.LEVELS.get(log_level, logging.NOTSET)
    logging.basicConfig(level=log_level)
    logger.setLevel(log_level)

    directory_results = config.PATH['experimets_results']+'/'+\
    my_experiment.name

    if not my_experiment.debug:
        if not os.path.exists(directory_results):
            os.makedirs(directory_results)
        fh = logging.FileHandler(directory_results+'/logfile.log')
        logger.addHandler(fh)
        logger.info(my_experiment)


    #logger.critical('This is a critical message.')
    #logger.error('This is an error message.')
    #logger.warning('This is a warning message.')
    #logger.info('This is an informative message.')
    #logger.debug('This is a low-level debug message.')

    return logger
