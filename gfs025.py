from gfs import Gfs
class Gfs025(Gfs):


    def __init__(self):

        import gfs025Data

        super(Gfs025, self).__init__()
        self.__resolution = 0.25

        self.level = gfs025Data.level
        self.variable = gfs025Data.variable
        self.region = gfs025Data.region

    def url (self, experiment_wrf, actual_date, actual_forecast):
        url = super(Gfs025, self).url()
        file_ = 'filter_gfs_0p25.pl?file=gfs.t%sz.pgrb2.0p25.f%s&'%\
        (experiment_wrf.start_date.strftime("%H"), str(actual_forecast).zfill(3))

        grib_url = reduce( lambda x, y: x+'&'+y , ['lev_'+x+'=on'  for x in self.level.keys() if self.level[x] ])
        grib_url += '&'+reduce( lambda x, y: x+'&'+y , ['var_'+x+'=on'  for x in self.variable.keys() if self.variable[x] ])

        grib_url += "&subregion=&"+ reduce( lambda x, y: x+'&'+y , ['%s=%s'%(x,self.region[x]) for x in self.region.keys() if self.region[x] ])

        grib_url += '&dir=%2Fgfs.'+experiment_wrf.start_date.strftime("%Y%m%d%H")

        return url, file_, grib_url

    def resolution(self):
        return '0.25'

    def forcing_patern(self):
        return self.name()+"_0.25_????????_??_???.grib2"

    def name(self):
        return 'gfs_operational'
